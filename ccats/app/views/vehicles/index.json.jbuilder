json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :customer_id, :make_id, :model_id, :year_id, :color_id, :engine_id, :transmission_id, :chassis_id
  json.url vehicle_url(vehicle, format: :json)
end
