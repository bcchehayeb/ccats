json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :job_id, :date, :comments
  json.url appointment_url(appointment, format: :json)
end
