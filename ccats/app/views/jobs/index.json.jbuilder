json.array!(@jobs) do |job|
  json.extract! job, :id, :vehicle_id, :employee_id, :invoice_id, :amount, :complete, :comments
  json.url job_url(job, format: :json)
end
