json.array!(@surplus_products) do |surplus_product|
  json.extract! surplus_product, :id, :job_id, :part_description, :image_url, :price
  json.url surplus_product_url(surplus_product, format: :json)
end
