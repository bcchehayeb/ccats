json.array!(@emp_jobs) do |emp_job|
  json.extract! emp_job, :id, :employee_id, :job_id
  json.url emp_job_url(emp_job, format: :json)
end
