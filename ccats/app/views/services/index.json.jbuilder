json.array!(@services) do |service|
  json.extract! service, :id, :desc, :price, :flag_hrs
  json.url service_url(service, format: :json)
end
