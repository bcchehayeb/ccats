json.array!(@chasses) do |chass|
  json.extract! chass, :id, :code
  json.url chass_url(chass, format: :json)
end
