json.extract! @invoice, :id, :customer_id, :date, :subtotal, :adjustment, :tax, :total, :paid, :created_at, :updated_at
