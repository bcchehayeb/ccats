class InvoicePdf < Prawn::Document

   def initialize(invoice, view)
    super()
    @invoice = invoice
    @view = view
 #   border_line
    logo
    invoice_date
    thanks_message
    invoice_subtotal
    invoice_adjustment
    invoice_tax
    invoice_total
    signature_line
  end

   def logo
     logopath =  "#{Rails.root}/app/assets/images/sponsor.logo.jpg"
     image logopath, :width => 197, :height => 91
     move_down 10
     draw_text "Invoice Receipt", :at => [220, 575], size: 22
   end

   def invoice_date
     text "Invoice Date:
     #{@invoice.date} ", :size => 13
     move_down 20
   end

    def thanks_message
     move_down 80
     text "Thank you #{@invoice.customer.fname.capitalize} #{@invoice.customer.lname.capitalize}, this is a receipt in confirmation of your service.",
     :indent_paragraphs => 40, :size => 13
   end



   def invoice_subtotal
     move_down 40
     text "Subtotal:
    #{@view.number_to_currency@invoice.subtotal} ", :size => 13,:align => :right
     move_down 20
   end
   def invoice_adjustment
     move_down 40
     text "Adjustment:
    #{@view.number_to_currency@invoice.adjustment} ", :size => 13,:align => :right
     move_down 20
   end



   def invoice_tax
     move_down 40
     text "Tax:
    #{@view.number_to_currency@invoice.tax} ", :size => 13,:align => :right
     move_down 20
   end

   def invoice_total
     move_down 40
     text "Invoice Total:
    #{@view.number_to_currency@invoice.total} ", :size => 13, :align => :right
     move_down 20
   end

   def signature_line
     move_down 40
    text "____________________________________________", :size => 13, :align => :right
     text "signature", :size => 8, :align => :right
     move_down 20
   end

end

def border_line
  stroke_color "ffffff"
  vertical_line 0, 1000, :at => 0
  vertical_line 800, 1000, :at => 0
  horizontal_line 0, 800, :at => 0
  horizontal_line 0, 800, :at => 0

end