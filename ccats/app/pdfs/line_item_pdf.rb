class Line_itemsPdf < Prawn::Document

  def initialize(line_item, view)
    super()
    @line_item = line_item
    @view = view
    logo
    thanks_message
  end

   def logo
     logopath =  "#{Rails.root}/app/assets/images/sponsor.logo.jpg"
     image logopath, :width => 197, :height => 91
     move_down 10
     draw_text "Job details", :at => [220, 575], size: 22
   end

  def  job_number
    text "Job Number:
     #{@job.id} ", :size => 13
    move_down 20
  end

   def thanks_message
     move_down 80
     text "Thank you, this is a listing of services provided.",
          :indent_paragraphs => 40, :size => 13
   end
end
