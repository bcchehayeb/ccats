class Customer < ActiveRecord::Base

  has_many :invoices
  has_many :vehicles


  def show_full_name
    "#{fname} #{lname}"
  end

end
