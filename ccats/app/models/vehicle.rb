class Vehicle < ActiveRecord::Base

  has_many :jobs

  belongs_to :customer
  belongs_to :make
  belongs_to :model
  belongs_to :year
  belongs_to :color
  belongs_to :engine
  belongs_to :transmission
  belongs_to :chassis


  def show_veh_info
    "#{self.customer.show_full_name}  - #{self.year.mod_year} #{self.make.name} #{self.model.name} #{self.color.name}"
  end

end