class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :fname, :length => 50
      t.string :lname, :length => 50
      t.string :phone, :length => 16
      t.string :email, :length => 100
      t.string :street, :length => 50
      t.string :city, :length => 50
      t.string :state, default:"TX", :length => 2
      t.string :zip, :length => 10

      t.timestamps null: false
    end
  end
end
