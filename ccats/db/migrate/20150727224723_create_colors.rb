class CreateColors < ActiveRecord::Migration
  def change
    create_table :colors do |t|
      t.string :name, :length => 100

      t.timestamps null: false
    end
  end
end
