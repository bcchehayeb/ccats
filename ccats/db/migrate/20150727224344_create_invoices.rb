class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :customer_id
      t.date :date
      t.decimal :subtotal, default: 0, :precision => 6, :scale => 2
      t.decimal :adjustment, default: 0, :precision => 6, :scale => 2
      t.decimal :tax, :precision => 6, :scale => 2
      t.decimal :total, :precision => 6, :scale => 2
      t.boolean :paid

      t.timestamps null: false
    end
  end
end
