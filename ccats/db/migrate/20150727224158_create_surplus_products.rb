class CreateSurplusProducts < ActiveRecord::Migration
  def change
    create_table :surplus_products do |t|
      t.integer :job_id
      t.text :part_description
      t.string :image_url, :length => 100
      t.decimal :price, :precision => 7, :scale => 2

      t.timestamps null: false
    end
  end
  def self.up
    change_table :surplus_products do |t|

      t.attachment :photo

    end
  end

  def self.down

    remove_attachment :surplus_products, :photo

  end
end
