class CreateTransmissions < ActiveRecord::Migration
  def change
    create_table :transmissions do |t|
      t.string :style, :length => 100

      t.timestamps null: false
    end
  end
end
