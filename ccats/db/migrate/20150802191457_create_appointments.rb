class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :job_id
      t.datetime :date
      t.text :comments

      t.timestamps null: false
    end
  end
end
